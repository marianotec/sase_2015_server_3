/**
 * DOXYGEN COMMENTS
 *
 * @file   s_udp.c
 *
 * @Author Koremblum, Nicolás Mariano (marianotec7@gmail.com)
 * 		   Di Donato, Andrés (adidonato@gmail.com)
 *
 * @date   31/7/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

/* **************************************************************** */
/* 								INCLUDEs							*/

#include "s_tcp_udp.h"

/* **************************************************************** */
/* 								DEFINEs								*/

// Add private define's here

/* **************************************************************** */
/* 								MACROs								*/

// Add private macro's here

/* **************************************************************** */
/* 								GLOBALs								*/

// Add global variable's here

/* **************************************************************** */
/* 								 CODE								*/

int udp_connectionClient (char * server_ip, int server_port, struct sockaddr_in * their_addr)
{
    /*
 *Esta funcion se conecta a un servidor udp en el puerto server_port y direccion server_ip
*/

    int	sockfd;
    struct hostent *he;	/* Se utiliza para convertir el nombre del host a su
                         dirección IP */

    /* Convertimos el nombre del host a su dirección IP */
    if ((he = (struct hostent *) gethostbyname ((const char *) server_ip)) == NULL)
    {
        herror("Error en gethostbyname \n");
        return -1;
    }

    /* Creamos el socket */
    if ((sockfd = socket(AF_INET,SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
        perror("Error en creación de socket \n");
        return -2;
    }

    /* Establecemos their_addr con la direccion del server */

    their_addr->sin_family = AF_INET;           /*familia de sockets INET para UNIX*/
    their_addr->sin_port = htons(server_port);  /*convierte el entero formato PC a
//                                               entero formato network*/

//    bzero((their_addr->sin_zero),8);           /* rellena con ceros el resto de la
//                                               estructura */

    if (inet_aton(server_ip , &their_addr->sin_addr) == 0)/* se carga la IP del servidor */
    {
        fprintf(stderr, "inet_aton() failed\n");
        return -1;
    }

    return sockfd;
}

//Esta funcion crea una conexion udp en el puerto server_port en la IP local
int udp_connectionServer ( int server_port, struct sockaddr_in * my_addr)
{
    /*
 *Esta funcion crea una conexion udp en el puerto server_port en la IP local
*/

    int	sockaux;	/*socket auxiliar*/
    int	aux;        /*variable auxiliar*/

    /*Crea un socket y verifica si hubo algún error*/
    if ((sockaux = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
        fprintf(stderr, "Error en función socket. Código de error %s \n", strerror(sockaux));
        return -1;
    }

    /* Asignamos valores a la estructura my_addr */

    my_addr->sin_family = AF_INET;              /*familia de sockets INET para UNIX*/
    my_addr->sin_port = htons(server_port);     /*convierte el entero formato PC a
//                                               entero formato network*/
    my_addr->sin_addr.s_addr = htonl(INADDR_ANY);      /* automaticamente usa la IP local */
    //bzero((my_addr->sin_zero),8);              /* rellena con ceros el resto de la
//                                               estructura */

    /* Con la estructura sockaddr_in completa, se declara en el Sistema que este
     proceso escuchará pedidos por la IP y el port definidos*/
    if ( (aux = bind (sockaux, (struct sockaddr *) my_addr, sizeof(struct sockaddr))) == -1)
    {
        fprintf(stderr, "Error en función bind. Código de error %s \n", strerror(aux));
        return -1;
    }
    return sockaux;
}

ssize_t udp_sendData(int fd, void * data, int bytes,struct sockaddr_in  dest_addr )
{
    ssize_t numbytes;

    if ((numbytes = sendto(fd, data, bytes, 0, (struct sockaddr *)&dest_addr, sizeof(struct sockaddr))) == -1)
    {
        perror("Error SENDTO UDP Server \n");
        exit(1);
    }
    return numbytes;
}

ssize_t udp_recvData(int fd, void * data, int bytes, struct sockaddr_in * src_addr)
{
    ssize_t numbytes;

    socklen_t addr_len;

    fd_set master_fds;

    struct timeval timeout;  //para el timeout

    int valor_select;

    addr_len = sizeof(struct sockaddr);

    FD_ZERO(&master_fds);               // vacio la lista del select

    FD_SET(fd, &master_fds);  // agrego el fd a al lista del select

    timeout.tv_sec = 1;
    timeout.tv_usec = 0;

    valor_select = select(fd + 1, &master_fds, NULL, NULL, &timeout);

    if(valor_select < 0)
    {
        perror("[error] recvDataUDP: fallo select");
        return -1;
    }
    if(FD_ISSET(fd, &master_fds))    // se puede leer sin bloquearme
    {
        if ((numbytes = recvfrom(fd, (char *)data, bytes, 0, (struct sockaddr *)src_addr, &addr_len)) == -1)
        {
            perror("[error] recvDataUDP: Error en recvfrom");
            return -1;
        }
        return numbytes;
    }

    if(valor_select == 0) // si se cumplio el tiempo
    {
        fprintf(stderr, "[error] recvDataUDP: timeout \n");
        return -1;
    }

    return -1;
}
