/**
 * DOXYGEN COMMENTS
 *
 * @file   s_main.c
 *
 * @Author Koremblum, Nicolás Mariano (marianotec7@gmail.com)
 * 		   Di Donato, Andrés (adidonato@gmail.com)
 *
 * @date   3/8/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

/* **************************************************************** */
/* 								INCLUDEs							*/

#include	"s_main.h"

/* **************************************************************** */
/* 								DEFINEs								*/

// Add private define's here

/* **************************************************************** */
/* 								MACROs								*/

#define		CLEAR_SCREEN		system("clear")

/* **************************************************************** */
/* 								GLOBALs								*/

// Add global variable's here

/* **************************************************************** */
/* 								 CODE								*/

int main(void)
{
	/** ************    VARIABLE's DECLARATION    ************ **/

	int tcpServerFD = -1;						// Variable donde se almacena el file descriptor del socket TCP del servidor

	int udpServerFD = -1;						// Variable donde se almacena el file descriptor del socket UDP del servidor

	struct sockaddr_in tcpServerAddress;		// Estructura que contiene información útil sobre el socket TCP del servidor

	struct sockaddr_in udpServerAddress;		// Estructura que contiene información útil sobre el socket UDP del servidor

	/** ****************************************************** **/

	signal(SIGINT,ctrlC_signalHandler);

	CLEAR_SCREEN;

	printf("\nDebug(Servidor): Abriendo sockets...\n");

	//Abro el socket UDPclear
	udpServerFD = udp_connectionServer( UDP_SERVER_PORT , &udpServerAddress );

	if(	IS_ERROR(udpServerFD)	)
	{
		printf("\nDebug(Servidor): Error UDP\n");

		return -1;
	}

	//Abro el socket TCP para recibir clientes
	tcpServerFD = tcp_openConnection( TCP_SERVER_PORT , &tcpServerAddress );

	if(	IS_ERROR(tcpServerFD)	)
	{
		printf("\nDebug(Servidor): Error TCP\n");

		return -1;
	}

	printf("\nDebug(Servidor): Los sockets se abrieron exitosamente.\n");

	father_serverProcess(tcpServerFD,udpServerFD,&tcpServerAddress,&udpServerAddress);

	return 0;
}
