/**
 * DOXYGEN COMMENTS
 *
 * @file   s_father.c
 *
 * @Author Koremblum, Nicolás Mariano (marianotec7@gmail.com)
 * 		   Di Donato, Andrés (adidonato@gmail.com)
 *
 * @date   3/8/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

/* **************************************************************** */
/* 								INCLUDEs							*/

#include	"s_main.h"

/* **************************************************************** */
/* 								DEFINEs								*/

// Add private define's here

/* **************************************************************** */
/* 								MACROs								*/

#define		ENDLINE2NULL		for(index=0;messageBuffer[index]!='\n'&&messageBuffer[index]!='\0';index++);messageBuffer[index]='\0';index=0

#define		write2Child(x)		writeTo(wPipeFD, (const char *) x)

/* **************************************************************** */
/* 								GLOBALs								*/

// Add global variable's here

/* **************************************************************** */
/* 								 CODE								*/

void father_serverProcess(int tcpServerFD, int udpServerFD, struct sockaddr_in *tcpServerAddress, struct sockaddr_in *udpServerAddress)
{
	/** ************    VARIABLE's DECLARATION    ************ **/

	int index = 0;								// Variable utilizada para recorrer vectores y como auxiliar en lazos

	int tcpClientFD = 0;

	int exitVar = FALSE;						// Variable que indica si se desea salir del loop principal o no

	int fixedMaxFD = 0;

	int selectValue = -1;						// Variable que toma el valor que retorna la función Select

	int childPid = 0;

	char messageBuffer[MAX_MESSAGE];			// Vector donde se guardan los mensajes recibidos

	fd_set fdsMaster;							// Variable que utiliza el handler del Select

	fd_set fdsAuxiliar;							// Variable que utiliza el handler del Select

	unsigned int count = 0;

	unsigned int addrSize = 0;

	struct sigaction sa;

	struct timeval timeout;						// Estructura para setear el timeout del Select

	struct sockaddr_in	udpClientAddress;

	struct sockaddr_in	tcpClientAddress;		// Estructura que contiene información útil sobre el socket TCP del cliente

	/** ****************************************************** **/

	sa.sa_handler = child_signalHandler;
	sa.sa_flags = SA_RESTART;
	sigemptyset(&sa.sa_mask);

	if(sigaction(SIGCLD,&sa,NULL)<0)
	{
		perror("sigaction");
		exit(1);
	}

	count = sizeof(udpClientAddress);

	/** ************    Seteos de la función Select    ************ **/

	FD_ZERO( &fdsMaster );

	//File Descriptos a monitorear
	FD_SET( KEYBOARD_FD, &fdsMaster );		//Seteo el teclado
	FD_SET( tcpServerFD, &fdsMaster );		//Seteo el socket TCP
	FD_SET( udpServerFD, &fdsMaster );		//Seteo el socket UDP

	fixedMaxFD = ((tcpServerFD > udpServerFD)?tcpServerFD:udpServerFD)+1;

	//fixedMaxFD = 1+tcpServerFD;

	//Seteos timeout
	timeout.tv_sec 	= SEC_SELECT_TIMEOUT;
	timeout.tv_usec = USEC_SELECT_TIMEOUT;

	/** ****************************************************** **/

	/**		MAIN LOOP		**/
	while(FALSE == exitVar)
	{
		bzero(messageBuffer,MAX_MESSAGE);		//Limpio el buffer de mensajes

		fdsAuxiliar = fdsMaster;		//Copio en Auxiliar el Master, dado que el mismo se "reinicia" en cada loop

		if ((selectValue = select( fixedMaxFD , &fdsAuxiliar , NULL , NULL , &timeout ) )>-1 )
		{
			/**
			 * SELECT STATEMENT: "Interrupción" por teclado
			 **/
			if( FD_ISSET( KEYBOARD_FD , &fdsAuxiliar ) == TRUE )
			{
				fgets(messageBuffer, MAX_MESSAGE, stdin);		//Capturo el comando ingresado por teclado

				ENDLINE2NULL;

				//Si las strings coinciden
				if(0 == strcmp(messageBuffer,"exit"))
				{
					printf("\nDebug(Proceso Padre): Se ingresó el comando de escape\n");

					exitFunction(&exitVar,0,"exit");
				}
			}

			/**
			 * SELECT STATEMENT: "Interrupción" por socketTCP
			 **/
			else if( FD_ISSET( tcpServerFD , &fdsAuxiliar ) == TRUE )
			{
				childPid = 0;

				//Abro el socket TCP para atender a un cliente
				tcpClientFD = tcp_connectionClient( tcpServerFD , &tcpClientAddress );

				if( IS_ERROR(tcpClientFD) )
				{
					printf("\nDebug(Proceso Hijo): Error TCP\n");

					exit (-1);
				}

				//Creo un proceso hijo
				childPid = fork();

				//Accedo a la función principal del proceso Hijo
				if( IS_CHILD_PROCCESS(childPid) )	child_serverProcess(tcpClientFD,&tcpClientAddress);

			}

			else if( FD_ISSET( udpServerFD , &fdsAuxiliar ) == TRUE )
			{
				recvfrom(udpServerFD,messageBuffer,MAX_MESSAGE,0,(struct sockaddr *)&udpClientAddress,(unsigned int *)&addrSize);

				bzero(messageBuffer,MAX_MESSAGE);

				sprintf(messageBuffer,"%d",count);

				ENDLINE2NULL;

				sendto(udpServerFD,messageBuffer,strlen(messageBuffer),0,(struct sockaddr *)&udpClientAddress,sizeof(udpClientAddress));
			}

			/**
			 * SELECT STATEMENT: "Interrupción" por timeout
			 **/
			else if( IS_TIMEOUT( selectValue ) )
			{
				count++;

				//Seteos timeout
				timeout.tv_sec 	= SEC_SELECT_TIMEOUT;
				timeout.tv_usec = USEC_SELECT_TIMEOUT;
			}

			else
			{
				//Seteos timeout
				timeout.tv_sec 	= SEC_SELECT_TIMEOUT;
				timeout.tv_usec = USEC_SELECT_TIMEOUT;
			}
		}

		else	perror("\nselect?: ");
	}
	printf("\nDebug(Proceso Padre): Cerrando la aplicación...\n");

	close(tcpServerFD);

	close(udpServerFD);

	return;
}
