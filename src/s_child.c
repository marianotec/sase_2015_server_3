/**
 * DOXYGEN COMMENTS
 *
 * @file   s_child.c
 *
 * @Author Koremblum, Nicolás Mariano (marianotec7@gmail.com)
 * 		   Di Donato, Andrés (adidonato@gmail.com)
 *
 * @date   3/8/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

/* **************************************************************** */
/* 								INCLUDEs							*/

#include	"s_main.h"

/* **************************************************************** */
/* 								DEFINEs								*/

// Add private define's here

/* **************************************************************** */
/* 								MACROs								*/

#define		write2Client(x)		writeTo(tcpClientFD, (const char *) x)

#define		write2Father(x)		writeTo(wPipeFD, (const char *) x)

/* **************************************************************** */
/* 								GLOBALs								*/

// Add global variable's here

/* **************************************************************** */
/* 								 CODE								*/

void child_serverProcess (int tcpClientFD, struct sockaddr_in * tcpClientAddress)
{
	/** ************    VARIABLE's DECLARATION    ************ **/

	int fixedMaxFD = -1;

	int exitVar = FALSE;							// Variable que indica si se desea salir del loop principal o no

	fd_set fdsMaster;							// Variable que utiliza el handler del Select

	fd_set fdsAuxiliar;							// Variable que utiliza el handler del Select

	char messageBuffer[MAX_MESSAGE];

	/** ****************************************************** **/

	/** ************    Seteos de la función Select    ************ **/

	FD_ZERO( &fdsMaster );

	FD_SET( tcpClientFD, &fdsMaster ); 	//Seteo el socket TCP

	fixedMaxFD = tcpClientFD + 1;

	/** ****************************************************** **/

	/**		MAIN LOOP		**/
	while(FALSE == exitVar)
	{
		bzero(messageBuffer,MAX_MESSAGE);

		fdsAuxiliar = fdsMaster; //Copio en Auxiliar el Master, dado que el mismo se "reinicia" en cada loop

		select( fixedMaxFD , &fdsAuxiliar , NULL , NULL , NULL );

		/**
		 * SELECT STATEMENT: "Interrupción" por socket TCP (Cliente)
		 **/
		if( FD_ISSET( tcpClientFD , &fdsAuxiliar ) == TRUE )
		{
			read(tcpClientFD, messageBuffer, MAX_MESSAGE);

			if(0 == strcmp(messageBuffer,"exit"))	exitFunction(&exitVar,0,"exit");
		}

		/**   END OF SELECT LOOP   **/
	}

	close(tcpClientFD);

	exit(0);
}
