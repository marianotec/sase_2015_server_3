/**
 * DOXYGEN COMMENTS
 *
 * @file   s_auxFunctions.c
 *
 * @Author Koremblum, Nicolás Mariano (marianotec7@gmail.com)
 * 		   Di Donato, Andrés (adidonato@gmail.com)
 *
 * @date   3/8/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

/* **************************************************************** */
/* 								INCLUDEs							*/

#include "s_auxFunctions.h"

/* **************************************************************** */
/* 								DEFINEs								*/

#define		TRUE		1

/* **************************************************************** */
/* 								MACROs								*/

#define		IS_ERROR(x)		( (0 > x)  ? 1:0 )

/* **************************************************************** */
/* 								GLOBALs								*/

// Add global variable's here

/* **************************************************************** */
/* 								 CODE								*/

void exitFunction(int *exitVar, int wFD, const char *message)
{
	int returnValue = 1;

	if(exitVar)	*exitVar = TRUE;

	if(wFD)	returnValue = writeTo(wFD, message);

	if(IS_ERROR(returnValue))	*exitVar = returnValue;

	return;
}



int writeTo(int wFD, const char * message)
{
	int returnValue = 0;

	returnValue = write(wFD, message, strlen(message));

	if(IS_ERROR(returnValue))
	{
		returnValue = write(wFD, message, strlen(message));

		if(IS_ERROR(returnValue))	return WRITE_ERROR_CODE;
	}

	return 0;
}

void ctrlC_signalHandler(int useless) //SIGINT
{
	printf("\n\nEsa no es la forma correcta de cerrar la aplicación, por favor tipee \"exit\"\n\n");
}

void child_signalHandler(int useless) //SIGCHLD
{
	wait(NULL);

	printf("\n->Se cerró correctamente un proceso hijo\n");
}
